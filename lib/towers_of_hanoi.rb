# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  attr_reader :towers

  def initialize
    @towers = [[3,2,1], [], []]
  end

  def play
    until won?
      get_disc_from
      move_disc_to
      valid_move?(get_disc_from, move_disc_to) ? move(get_disc_from, move_disc_to) : invalidmove
    end
    puts "Congratulations! You Won!"
  end

  def get_disc_from
    print_tower
    puts "What pile would you like to select a disc from? Please select 1, 2, or 3 (from left to right)"
    answer = gets.to_i
    valid_input?(answer) ? answer - 1 : user_input_error
  end

  def move_disc_to
    puts "Which pile would you like to move the disc to? Please select 1, 2, or 3 (from left to right)"
    answer = gets.to_i
    valid_input?(answer) ? answer - 1 : user_input_error
  end

  def print_tower
    @towers.each_with_index do |tower, idx|
      if tower.empty?
        puts "Tower #{idx+1}: empty"
      else
        puts "Tower #{idx+1}: #{tower}"
      end
    end
  end

  def valid_input?(answer)
    [1,2,3].include?(answer)
  end

  def user_input_error
    puts "Invalid answer, please select a number from 1 to 3"
  end

  def valid_move?(get_disc_from, move_disc_to)
    return true if @towers[move_disc_to].empty?
    return false if @towers[get_disc_from].empty?
    @towers[get_disc_from].last < @towers[move_disc_to].last
  end

  def invalid_move
    puts "Invalid move, distination tower is smaller than the disc wished to be moved"
  end

  def move(get_disc_from, move_disc_to)
    @towers[move_disc_to] << (@towers[get_disc_from].pop)
  end

  def won?
    towers_aligned?(1) || towers_aligned?(2)
  end

  def towers_aligned?(idx)
    @towers[idx] == [3,2,1]
  end
end


# game = TowersOfHanoi.new
# game.play
